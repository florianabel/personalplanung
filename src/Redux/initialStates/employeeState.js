const employeeState = {
  _id: null,
  firstname: null,
  lastname: null,
  dateOfBirth: null,
  weekhours: null,
  leaveDays: null,
  qualifications: {},
  groups: {azubi: false, group1: false, group2: false},
  calender: {}
};

export default employeeState;
