// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { DatePicker } from 'material-ui-pickers';

// Importing components
import PreferenceCalender from './PreferenceCalender';
import CreateRule from './CreateRule';

// const for Material-UI
const styles = theme => ({
  textField: {
    margin: theme.spacing.unit,
  }
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class Calender extends Component {
  constructor(props) {
      super(props);
      this.state = {
        calender: {}
      };
    }

  componentDidMount() {

  }

  handleDateChange = date => {
    this.props.yearChange('selectedYear', date);
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <>
      <h4>Kalender</h4>
      <DatePicker
        openToYearSelection
        className={classes.textField}
        keyboard
        mask={[ /\d/, /\d/, /\d/, /\d/]}
        label="Kalenderjahr"
        format="YYYY"
        value={this.props.selectedYear}
        onChange={this.handleDateChange}
        variant="outlined"
      />
      <CreateRule />
      <PreferenceCalender
        calender={this.props.calender}
        preferenceChange={(week, day, date, preference) => this.props.preferenceChange(week, day, date, preference)}
       />
    </>
  );
}}

Calender.propTypes = {classes: PropTypes.object.isRequired};
Calender = connect(mapStateToProps, mapDispatchToProps)(Calender);
export default (withStyles(styles)(Calender));
