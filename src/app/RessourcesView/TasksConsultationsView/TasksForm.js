// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

// Importing components

const styles = theme => ({
  taskForm: {
    'margin-left': 'auto',
    'margin-right': 'auto',
    width: 'calc(80%)',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

class TasksForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        
      };
    }

  componentDidMount() {

  }

  handleChange = key => event => {
    let value = event.target.value;
    if (key === 'weekhours' && value < 0) {
      value = 0;
    }
    this.props.stateChange(key, value);
  };

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
  const { classes } = this.props;

  return (
    <form className={classes.taskForm} noValidate autoComplete="off">
      <TextField
      fullWidth
      required
      id="taskName"
      label='Name'
      value={this.props.taskName}
      onChange={this.handleChange('taskName')}
      margin="normal"
      variant="outlined"
      /><br/>
      <Button variant="outlined" color="primary" className={classes.button}>
        Speichern
      </Button>
      <Button variant="outlined" color="primary" className={classes.button}>
        Abbrechen
      </Button>
    </form>
  );
}}

TasksForm.propTypes = {classes: PropTypes.object.isRequired};
TasksForm = connect(mapStateToProps, mapDispatchToProps)(TasksForm);
export default (withStyles(styles)(TasksForm));
