// Importing React
import React, { Component } from 'react';

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Icon from '@material-ui/core/Icon';
import { IconButton } from '@material-ui/core';

// Importing components
import TaskElement from './TaskElement';


const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit,
    paddingBottom: 0,
  },
  consultationButton: {
    float: 'right',
    width: 20,
    height: 20,
  },
  consultationIcon: {
    fontSize: 18,
  },
  noMargin: {
    margin: 0,
    padding: 0
  }
});

class ConsultationElement extends Component {
  constructor(props) {
      super(props);
      this.state = {

      };
  }

//----------------------------CALLBACK-FUNCTIONS---------------------------//
//-------------------------------------------------------------------------//



//-------------------------------HELPER-FUNCTIONS--------------------------//
//-------------------------------------------------------------------------//

  getTimeString = (timestamp) => {
    let hours = timestamp.getHours();
    let minutes = timestamp.getMinutes();
    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    let timeString = hours + ':' + minutes;
    return (timeString)
  }

  getTimeRange = (consultation) => {
    let consultationStart = this.getTimeString(consultation.consultationStart)
    let consultationEnd = this.getTimeString(consultation.consultationEnd)
    let timeRange;
    if (consultationStart === consultationEnd) {
      timeRange = 'Ganztägig'
    } else {
      timeRange = consultationStart + ' - ' + consultationEnd;
    }
    return (timeRange);
  }

// UGLY AS FUCK!!!! ----> REPLACE AS SOON AS POSSIBLE!!!!
// PROBLEM IS REFERRING TO TASKS BY KEY PROPERTY INSTEAD OF // ID
// --> WHEN DYNAMICALLY CREATING TASKS --> MAP TASKS TO SERVICES BY DATABASE-ID
// INSTEAD OF KEY LIKE "recorder"
  findTask = (task) => {
    let keys = Object.keys(this.props.tasks);
    let taskName = "error";
    keys.forEach(key => {
      if (this.props.tasks[key].key === task) {
        taskName = this.props.tasks[key].name;
      }
    });
    return (taskName);
  }

//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render() {
    const { classes, consultation, employees } = this.props;
    const keysTasks = Object.keys(consultation.tasks);
    //console.log(this.props.name);
    return (
      <Paper className={classes.root}>
          <List dense={true}>
          <ListItem dense={true} className={classes.headline}>
            <ListItemText disableTypography>
            <Typography align="center" color="primary" variant="subheading">
              {this.props.name}
            </Typography>
            </ListItemText>
            <ListItemSecondaryAction>

              {/* Clear Consultation Button */}
              <IconButton className={classes.consultationButton}
              onClick={() => this.props.handleDeleteConsultation(consultation.id)}
              aria-label='Delete'>
                <Icon color='action' className={this.props.classes.consultationIcon}>
                  clear
                </Icon>
              </IconButton>

              {/* Edit Consultation Button */}
              <IconButton className={classes.consultationButton}
              onClick={() => this.props.handleEditConsultation(consultation.id)} aria-label='Update'>
                <Icon color='action' className={this.props.classes.consultationIcon}>
                  create
                </Icon>
              </IconButton>

            </ListItemSecondaryAction>
          </ListItem>
          </List>

        <Typography variant="body1" gutterBottom align="center" className={classes.noMargin}>
          {this.getTimeRange(consultation)}
        </Typography>

        <List dense={true} className={classes.noMargin}>
          {keysTasks.map(task =>
            <TaskElement key={task}
            taskKey={task}
            name={this.findTask(task)}
            room={consultation.tasks[task].room}
            employeeIds={consultation.tasks[task].employees}
            employees={employees}
            selectedTask={this.props.selectedTask}
            handleDrop={(taskKey, employeeId, position) => this.props.handleDrop(this.props.consultation.id, taskKey, employeeId, position)}
            handleSelectTask={(taskKey) => this.props.handleSelectTask(taskKey)}
            />
          )}
        </List>
      </Paper>
    )
  }
}

ConsultationElement.propTypes = {classes: PropTypes.object.isRequired};

export default (withStyles(styles)(ConsultationElement));
