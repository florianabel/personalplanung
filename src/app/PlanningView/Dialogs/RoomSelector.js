// Importing React
import React, { Component } from 'react';

// Importing Redux
import {connect} from 'react-redux';

// Import Redux actions

// Importing Material-UI
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

// Importing components

// const for Material-UI
const styles = theme => ({
  root: {
    flexGrow: 1,
    },

});

const mapStateToProps = (state) => {
  return {

    }
  };

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


class RoomSelector extends Component {
  constructor(props) {
      super(props);
      this.state = {
        labelWidth: 0,
      };
    }

    componentDidMount() {

      }

      handleChange = name => event => {
      this.setState({ [name]: event.target.checked });
    };


//----------------------------------RENDER---------------------------------//
//-------------------------------------------------------------------------//

  render () {
    const { classes } = this.props;

    return (
      <FormControl fullWidth>
      {this.props.label}
        <NativeSelect
          value={this.props.value}
          onChange={this.handleChange(this.props.stateKey)}
          name={this.props.name}
        >
          <option value={0}></option>
          <option value={1}>VU1</option>
          <option value={2}>VU2</option>
          <option value={3}>VU3</option>
        </NativeSelect>
       </FormControl>
    );
  }
}

RoomSelector.propTypes = {classes: PropTypes.object.isRequired};
RoomSelector = connect(mapStateToProps, mapDispatchToProps)(RoomSelector);
export default (withStyles(styles)(RoomSelector));
